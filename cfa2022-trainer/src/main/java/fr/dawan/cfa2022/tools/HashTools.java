package fr.dawan.cfa2022.tools;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Enumeration;
import java.util.Iterator;

import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPMessage;
import com.novell.ldap.LDAPResponse;
import com.novell.ldap.LDAPSearchConstraints;
import com.novell.ldap.LDAPSearchQueue;
import com.novell.ldap.LDAPSearchResult;
import com.novell.ldap.LDAPSearchResultReference;

import de.mkammerer.argon2.Argon2Factory;

public class HashTools {

	// chaine =hachage=> chaine non déchiffrable
	public static String hashSHA512(String input) throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-512");// singleton unique
		md.reset(); // réinitialiser le contenu du msg digest

		// application de l'algorithme choisi
		byte[] hachedArray = md.digest(input.getBytes("utf-8"));

		// conversion du message haché (tab de bytes) en une représentation numérique
		// signée
		BigInteger bi = new BigInteger(1, hachedArray);
		return String.format("%0128x", bi);
	}

	public static String hashArgon2(String input) throws Exception {
		return Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2id, 16, 32).hash(4, 65536, 1,
				input.getBytes(StandardCharsets.UTF_8));
	}

	public static void main(String[] args) {
		final String rawPass ="";

//		try {
//			System.out.println(hashArgon2(rawPass));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		int ldapPort = LDAPConnection.DEFAULT_SSL_PORT;
		int ldapVersion = LDAPConnection.LDAP_V3;
		String attrs[] = { "cn", "sn" };

		LDAPConnection lc = new LDAPConnection();

		String ldapHost = args[0];
		String loginDN = args[1];
		String password = args[2];
		String searchBase = args[3];
		String searchFilter = args[4];

		try {
			// connect to the server
			lc.connect(ldapHost, ldapPort);
			// authenticate to the server
			lc.bind(ldapVersion, loginDN, password.getBytes("UTF8"));
			// asynchronous search
			LDAPSearchQueue queue = null;
			// lc.search( searchBase, // container to search
//                        LDAPConnection.SCOPE_ONE// search container only 
//searchFilter,   // search filter
//attrs,          // return cn and sn
//false,          // return attrs and values                                      
//(LDAPSearchQueue)null,
//              // use default search queue
//(LDAPSearchConstraints)null);
// use default search constraints
			LDAPMessage message;

			while ((message = queue.getResponse()) != null) {
				// the message is a search result reference
				if (message instanceof LDAPSearchResultReference) {
					String urls[] = ((LDAPSearchResultReference) message).getReferrals();
					System.out.println("Search result references:");
					for (int i = 0; i < urls.length; i++)
						System.out.println(urls[i]);
				} else if (message instanceof LDAPSearchResult) { // the message is a search result
					LDAPEntry entry = ((LDAPSearchResult) message).getEntry();
					System.out.println("\n" + entry.getDN());
					System.out.println("\tAttributes: ");
					LDAPAttributeSet attributeSet = entry.getAttributeSet();
					Iterator allAttributes = attributeSet.iterator();

					while (allAttributes.hasNext()) {
						LDAPAttribute attribute = (LDAPAttribute) allAttributes.next();
						String attributeName = attribute.getName();
						System.out.println("\t\t" + attributeName);
						Enumeration allValues = attribute.getStringValues();
						if (allValues != null) {
							while (allValues.hasMoreElements()) {
								String Value = (String) allValues.nextElement();
								System.out.println("\t\t\t" + Value);
							}
						}
					}
				}
				// the message is a search response
				else {
					LDAPResponse response = (LDAPResponse) message;
					int status = response.getResultCode();
					// the return code is LDAP success
					if (status == LDAPException.SUCCESS) {
						System.out.println("Asynchronous search succeeded.");
					}
					// the reutrn code is referral exception
					else if (status == LDAPException.REFERRAL) {
						String urls[] = ((LDAPResponse) message).getReferrals();
						System.out.println("Referrals:");
						for (int i = 0; i < urls.length; i++)
							System.out.println(urls[i]);
					} else {
						System.out.println("Asynchronous search failed.");
						throw new LDAPException(response.getErrorMessage(), status, response.getMatchedDN());
					}
				}
			}
			// disconnect with the server
			lc.disconnect();

		} catch (LDAPException e) {
			System.out.println("Error: " + e.toString());
		} catch (UnsupportedEncodingException e) {
			System.out.println("Error: " + e.toString());
		}

		
		
		
		
		
	}
	
}
