package fr.dawan.cfa2022.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.dawan.cfa2022.entities.Article;
import fr.dawan.cfa2022.entities.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

	
}
