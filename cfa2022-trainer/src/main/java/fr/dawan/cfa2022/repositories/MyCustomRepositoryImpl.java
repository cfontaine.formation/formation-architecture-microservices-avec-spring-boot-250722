package fr.dawan.cfa2022.repositories;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import fr.dawan.cfa2022.entities.User;

@Repository
public class MyCustomRepositoryImpl implements MyCustomRepository {
	
	@PersistenceContext
	private EntityManager em; //objet connexion récupéré depuis le conteneur Spring
	
	@Override
	public List<User> searchBy(Map<String, String[]> params){
		Query q = em.createQuery("FROM XXXXXXX");
		//.... la suite de votre traitement
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> countBy(LocalDateTime startDate, LocalDateTime endDate, String city, Status status, Periodicity periodicity) {
		
		String clauseSelect = "SELECT COUNT(c.id) as nb";
		String clauseFrom = " FROM complaint c";
		String clauseWhere = " WHERE c.creation_date BETWEEN :startDate AND :endDate";
		String clauseGroupBy = " GROUP BY";
		String clauseOrderBy = "";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("startDate", startDate);
		params.put("endDate", endDate);
		
		if(city!=null && !city.trim().contentEquals("")) {
			clauseSelect += ", c.training_city";
			clauseWhere += "AND c.training_city = :city";
			
			if(!clauseGroupBy.equals(" GROUP BY")) 
				clauseGroupBy += ",";
			
			clauseGroupBy += " c.training_city";
			params.put("city", city);
		}

		if(status!=null && !status.toString().trim().contentEquals("")) {
			clauseSelect += ", c.status";
			clauseWhere += "AND c.status = :status";
			
			if(!clauseGroupBy.equals(" GROUP BY")) 
				clauseGroupBy += ",";
			
			clauseGroupBy += " c.status";
			params.put("status", status.toString());
		}
		
		if(periodicity!=null && !periodicity.toString().trim().contentEquals("")) {
			
			switch (periodicity) {
			case ANNUAL:
				clauseSelect += ", YEAR(c.creation_datetime)";
				if(!clauseGroupBy.equals(" GROUP BY")) 
					clauseGroupBy += ",";
				
				clauseGroupBy += " YEAR(c.creation_datetime)";
				break;
				
			case MONTHLY:
				clauseSelect += ", YEAR(c.creation_datetime), MONTH(c.creation_datetime)";
				if(!clauseGroupBy.equals(" GROUP BY")) 
					clauseGroupBy += ",";
				
				clauseGroupBy += " YEAR(c.creation_datetime), MONTH(c.creation_datetime)";
				break;

			case QUARTER:
				clauseSelect += ", YEAR(c.creation_datetime), QUARTER(c.creation_datetime)";
				if(!clauseGroupBy.equals(" GROUP BY")) 
					clauseGroupBy += ",";
				
				clauseGroupBy += " YEAR(c.creation_datetime), QUARTER(c.creation_datetime)";
				break;

			default:
				break;
			}
			
			
			
		}
		
		
		
		StringBuilder sqlString = new StringBuilder();
		sqlString.append(clauseSelect).append(clauseFrom).append(clauseWhere);
		if(!clauseGroupBy.equals(" GROUP BY")) {
			sqlString.append(clauseGroupBy);
		}
		
		sqlString.append(clauseOrderBy);
		Query q = em.createNativeQuery(sqlString.toString());
		
		for(String paramKey : params.keySet()) {
			q.setParameter(paramKey, params.get(paramKey));
		}
		
		
		return q.getResultList();
				
	}
}
