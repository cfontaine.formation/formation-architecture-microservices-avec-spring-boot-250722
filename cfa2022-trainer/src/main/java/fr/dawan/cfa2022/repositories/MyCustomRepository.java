package fr.dawan.cfa2022.repositories;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import fr.dawan.cfa2022.entities.User;

public interface MyCustomRepository {

	public List<User> searchBy(Map<String, String[]> params);
	
	public enum Status { IN_PROGRESS, DONE, CANCELLED };
	public enum Periodicity { ANNUAL, MONTHLY, QUARTER };
	
	public List<Object[]> countBy(LocalDateTime startDate, LocalDateTime endDate, String city, Status status, Periodicity periodicity);
}
