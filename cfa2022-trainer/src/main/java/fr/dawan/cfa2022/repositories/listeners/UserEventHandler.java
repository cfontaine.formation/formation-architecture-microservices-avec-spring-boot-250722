package fr.dawan.cfa2022.repositories.listeners;

import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

import fr.dawan.cfa2022.entities.User;

@RepositoryEventHandler(User.class)
public class UserEventHandler {
	@HandleBeforeSave
    public void handleAuthorBeforeCreate(User u){
        System.out.println("Inside User u Before Create....");
        String name = u.getFirstName();
        System.out.println(name);
    }

    @HandleAfterSave
    public void handleAuthorAfterCreate(User u){
    	System.out.println("Inside Author After Create ....");
        String name = u.getFirstName();
        System.out.println(name);
    }
    
//    Before* Event Annotations – associated with before annotations are called before the event is called.
// écrire @HandleBeforeXXXX
    
    
//    BeforeCreateEvent
//    BeforeDeleteEvent
//    BeforeSaveEvent
//    BeforeLinkSaveEvent
//
//After* Event Annotations – associated with after annotations are called after the event is called.
//écrire @HandleAfterXXXX
    
//    AfterLinkSaveEvent
//    AfterSaveEvent
//    AfterCreateEvent
//    AfterDeleteEvent


}
