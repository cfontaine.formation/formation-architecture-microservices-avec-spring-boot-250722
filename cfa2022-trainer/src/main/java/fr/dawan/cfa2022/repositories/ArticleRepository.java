package fr.dawan.cfa2022.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.cfa2022.entities.Article;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

	@Query("FROM Article a JOIN FETCH a.categories c WHERE c.id = :categoryId")
	List<Article> findAllByCategoryId(@Param("categoryId")long categoryId);
	
	
}
