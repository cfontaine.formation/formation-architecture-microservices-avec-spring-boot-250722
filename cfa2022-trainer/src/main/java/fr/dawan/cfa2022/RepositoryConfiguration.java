package fr.dawan.cfa2022;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.dawan.cfa2022.repositories.listeners.UserEventHandler;

@Configuration
public class RepositoryConfiguration {
	public RepositoryConfiguration(){
        super();
    }

    @Bean
    UserEventHandler userEventHandler() {
        return new UserEventHandler();
    }
}
