package fr.dawan.cfa2022.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController //Contrôleur qui représente un service web
//@RequestMapping("/test1")
public class ProductController {

	//GET : /test1
	@GetMapping(value="/test1", produces="text/html")
	public String test1() {
		return "<h1>test</h1>";
	}
	
	//GET : /test1/1
	@GetMapping(value="/test1/{param1}", produces="text/plain")
	public String test1WithPathVar(@PathVariable("param1") int p1) {
		return "Test " + p1;
	}
	
	// GET : /test1/search?qst=toto
	@GetMapping(value="/test1/search", produces="text/plain")
	public String test1WithReqParam(@RequestParam("qst") String p1) {
		return "Test search : " + p1;
	}
}

