package fr.dawan.cfa2022.listeners;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import fr.dawan.cfa2022.events.CategoryEvent;

@Component
public class CategoryListener implements ApplicationListener<CategoryEvent> {

	@Override
	public void onApplicationEvent(CategoryEvent event) {
		System.out.println("Received Category event  : " + event.getWhat());	
	}

}
