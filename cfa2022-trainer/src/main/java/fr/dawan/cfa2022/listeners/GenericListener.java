package fr.dawan.cfa2022.listeners;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import fr.dawan.cfa2022.events.CustomEvent;

@Component
public class GenericListener<T> implements ApplicationListener<CustomEvent<T>> {

	@Override
	public void onApplicationEvent(CustomEvent<T> event) {
		System.out.println("Generic Received event  : " + event.getWhat());
	}

}
