package fr.dawan.cfa2022.listeners;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import fr.dawan.cfa2022.entities.User;
import fr.dawan.cfa2022.events.CustomEvent;

@Component
public class UserListener2 implements ApplicationListener<CustomEvent<User>> {

	@EventListener
	@Override
	public void onApplicationEvent(CustomEvent<User> event) {
		System.out.println("Received event (listener2) : " + event.getWhat());	
	}

}
