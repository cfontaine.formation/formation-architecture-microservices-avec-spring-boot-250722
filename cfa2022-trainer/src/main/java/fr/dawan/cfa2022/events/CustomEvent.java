package fr.dawan.cfa2022.events;

import org.springframework.context.ApplicationEvent;

@SuppressWarnings("serial")
public class CustomEvent<T> extends ApplicationEvent {
	private T what;
	protected boolean success;

	public CustomEvent(Object source, T what) {
		super(source);
		this.what = what;
	}

	public T getWhat() {
		return what;
	}

	public void setWhat(T what) {
		this.what = what;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
