package fr.dawan.cfa2022.events;

import org.springframework.context.ApplicationEvent;

import fr.dawan.cfa2022.entities.Category;

@SuppressWarnings("serial")
public class CategoryEvent extends ApplicationEvent {
	private Category what;
	public CategoryEvent(Object source, Category what) {
		super(source);
		this.what = what;
	}

	public Category getWhat() {
		return what;
	}

	public void setWhat(Category what) {
		this.what = what;
	}

	
}
