package fr.dawan.cfa2022.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.cfa2022.dto.UserDto;
import fr.dawan.cfa2022.interceptors.TokenInterceptor;
import fr.dawan.cfa2022.services.UserService;
import fr.dawan.cfa2022.tools.HashTools;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

	@Autowired
	private UserController userController;
	
	@Autowired //objet pour lancer des requêtes MVC
	private MockMvc mockMvc;
	
	@MockBean
	private TokenInterceptor tokenInterceptor;
	
	@MockBean
	private UserService userService;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	private List<UserDto> users = new ArrayList<UserDto>();
	
	
	@BeforeEach //réaliser un traitement avant chaque test
	public void beforeEach() throws Exception {
		//quand il y a un appel à l'intercepteur, on retourne true
		when(tokenInterceptor.preHandle(any(), any(), any())).thenReturn(true);
		
		//on prévoit des données de test. 
		users.add(new UserDto(1L, "Mohamed", "DERKAOUI", "mderkaoui@dawan.fr", 
				HashTools.hashSHA512("secret"), true, "ADMIN", 0));
		
		users.add(new UserDto(2L, "User1", "User1LastName", "toto@dawan.fr", 
				HashTools.hashSHA512("secret"), true, "USER", 0));
	}
	
	//Vérifier que mon contrôleur est chargé correctement
	@Test
	void contextLoads() {
		assertThat(userController).isNotNull();
	}
	
	//tester le findAll GET : /api/users
	@Test
	void testFindAll() throws Exception {
		//s'il y a un appel à userService.getAll() ==> retourner la liste "users"
		when(userService.getAll()).thenReturn(users);
		
		mockMvc.perform(get("/api/users").accept(MediaType.APPLICATION_JSON))
			   .andExpect(status().isOk())
			   .andExpect(jsonPath("$.size()", is(users.size())))
		       .andExpect(jsonPath("$[0].firstName", is(users.get(0).getFirstName())));
	}
	
	@Test
	void testFindById() throws Exception {
		//s'il y a un appel à userService.getById(2) ==> retourner l'élément de la liste (et non req vers la Bdd)
		int userId = 2;
		when(userService.getById(userId)).thenReturn(users.get(1));
		
		//récupérer la réponse sous forme de chaine de caractères
		//la convertir en un objet Dto
		//comparer les 2 objets celui en réponse et celui dans la liste initiale
		
		mockMvc.perform(get("/api/users/{id}",userId).accept(MediaType.APPLICATION_JSON))
			   .andExpect(status().isOk())
			   .andExpect(jsonPath("$.id", is(userId)))
		       .andExpect(jsonPath("$.firstName", is(users.get(1).getFirstName())));
	}
	
	@Test
	void testDeleteById() throws Exception {
		long userId = 2L;
		//ne pas supprimer en Bdd quand tu réalises le test
		doNothing().when(userService).delete(userId);
		
		String res = mockMvc.perform(delete("/api/users/{id}",userId).accept(MediaType.APPLICATION_JSON))
			   .andExpect(status().isOk())
			   .andReturn().getResponse().getContentAsString();
		
		//si vous neutralisez le doNothing, il supprimera physiquement de la Bdd
		//et dans ce cas là, pour vérifier qu'il a bien supprimé l'élément, on peut aller chercher en Bdd
		//et vérifier que l'élément n'existe plus.
//		if(res==String.valueOf(userId)) {
//			UserDto resW = userService.getById(userId);
//			assertNull(resW);
//		}
		assertEquals(String.valueOf(userId), res);
		
	}
	
	
	//tester le insérer
	@Test
	void testInsert() throws Exception {
		UserDto userToInsert = new UserDto(0L, "Mohamed3", "DERKAOUI3", "mderkaoui3@dawan.fr", 
				HashTools.hashSHA512("secret"), true, "ADMIN", 0);
		
		objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
		String userToInsertStr = objectMapper.writeValueAsString(userToInsert);
		
		when(userService.saveOrUpdate(userToInsert)).thenReturn(userToInsert);
		
		mockMvc.perform(post("/api/users")
				.contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
				.content(userToInsertStr)
				.accept(MediaType.APPLICATION_JSON))
			   .andExpect(status().isCreated());
	}
	
	
}
