package fr.dawan.springmvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/mvc")
public class ExempleControllerMvc {
    

    @GetMapping("/jsp/{message}")
    public String exempleVueJsp(@PathVariable String message,Model model) {
        model.addAttribute("msg",message);
        return "exemplevuejsp";
    }
}
