package fr.dawan.formation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        //Interface méthode par défaut
        Voiture v1=new Voiture();
        v1.deplacer();
        v1.accelerer();
        System.out.println(Vehicule.getPuissanceMoteur());
       // System.out.println(Voiture.getPuissanceMoteur());
  
        // Expression Lambda
        // 1. classe qui implémente l'interface
        int t[]= {2,35,-3,18,1,9,10};
        Superieur sup=new Superieur();
        System.out.println(getValeurCmp(t, sup));
        
        // 2. classe annonyme
        System.out.println(getValeurCmp(t, new ICompare<Integer>() {
            
            @Override
            public boolean compare(Integer v1, Integer v2) {
                 return v1>v2;
            }
        }));
        
        // 3. Expression Lamda
        System.out.println(getValeurCmp(t,(a,b) -> { return a > b;}));
        System.out.println(getValeurCmp(t,(a,b) -> a < b));
        
        String sep=";";
        Arrays.asList("a", "b", "d").forEach(e->System.out.print(e + sep));
        
        Thread th1 = new Thread(() -> System.out.println("hello Wolrd !!"));
        th1.start();
        
        
        
        List<Integer> lst =Arrays.asList(3,3,3,6,1,-3,8,15);
        
        for( int i :lst) {
            System.out.println(i);
        }
        
        lst.forEach(i -> System.out.println(i));
        
        // Référence de méthode
        lst.forEach(System.out::println);
        System.out.println("---");
        // Stream
        lst.stream().filter(e->e>0 && e<10).map(m -> m*2).distinct().sorted().limit(2).forEach(System.out::println);// (a,b)->a>b?1:a==b?0:-1
        System.out.println("---");
        List<Integer> li=lst.stream().filter(e->e>0 && e<10).map(m -> m*2).distinct().sorted().limit(2).collect(Collectors.toList());
        li.forEach(System.out::println);
        int res=lst.stream().filter(e->e>0 && e<10).map(m -> m*2).distinct().sorted().limit(2).reduce(0,(a,b)->a-b);
        System.out.println(res);
  
    
        List<Produit> lstProd=new ArrayList<>();
        
        List<Produit> lstRes=lstProd.stream()
                .filter( p -> p.getPrix()>100.0)
                .filter(p -> p.getCategorie().equals("Livre")).collect(Collectors.toList());
        
        lstRes=lstProd.stream()
                .filter(p -> p.getCategorie().equals("Téléphone"))
                .peek(p -> p.setPrix(p.getPrix()*1.1)).collect(Collectors.toList());
        
        
        // Optional
        
        Optional<String> optVide=Optional.empty();
        System.out.println(optVide.isPresent());
        Optional<String> opt1=Optional.of("Hello World");
        System.out.println(opt1.isPresent());
        
        System.out.println(opt1.get());
        //System.out.println(optVide.get());
        
        optVide.ifPresent(System.out::println);
        opt1.ifPresent(System.out::println);
        
        System.out.println(optVide.orElse("default"));
        System.out.println(opt1.orElse("default"));
        try {
            System.out.println(optVide.orElseThrow(Exception::new));
        } catch (Exception e1) {
            System.out.println("vide");
        }
    }
    
    public static int getValeurCmp(int[] tab, ICompare<Integer> cmp) {
        int vl=tab[0];
        for(int v : tab) {
            if(cmp.compare(v, vl)) {
                vl=v;
            }
        }
        return vl;
    }

}
