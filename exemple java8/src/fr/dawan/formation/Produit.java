package fr.dawan.formation;

import java.time.LocalDate;

public class Produit {
    
    private String description;
    
    private double prix;
    
    private String categorie;
    
    private LocalDate dateProduction;

    public Produit() {
    }

    public Produit(String description, double prix, String categorie, LocalDate dateProduction) {
        this.description = description;
        this.prix = prix;
        this.categorie = categorie;
        this.dateProduction = dateProduction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public LocalDate getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(LocalDate dateProduction) {
        this.dateProduction = dateProduction;
    }

    @Override
    public String toString() {
        return "Produit [description=" + description + ", prix=" + prix + ", categorie=" + categorie
                + ", dateProduction=" + dateProduction + "]";
    }
    
    

}
