package fr.dawan.formation;

@FunctionalInterface
public interface ICompare<T> {
   boolean compare(T v1, T v2);
   
 //  void erreur();
}
