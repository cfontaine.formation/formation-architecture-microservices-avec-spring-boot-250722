package fr.dawan.formation;

public class Voiture implements Vehicule, Element {

    @Override
    public void deplacer() {
        System.out.println("Déplacer");
    }

    @Override
    public void accelerer() {
        System.out.println("la voiture accelère");
    }
}
