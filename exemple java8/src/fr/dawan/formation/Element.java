package fr.dawan.formation;

public interface Element {
    
    default void accelerer() {
        System.out.println("accelerer Element méthode par défaut");
    }
}
