package fr.dawan.formation;

public interface Vehicule {

    int NB_PORTE = 4; // par défaut: final static public

    static int getPuissanceMoteur() {
        return 6;
    }
    
    void deplacer();

    default void accelerer() {
        System.out.println("accelerer  Vehicule méthode par défaut");
    }
}
