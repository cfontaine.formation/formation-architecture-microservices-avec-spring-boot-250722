package fr.dawan.formation;

public class Superieur implements ICompare<Integer> {

    @Override
    public boolean compare(Integer v1, Integer v2) {
        return v1 > v2;
    }

}
