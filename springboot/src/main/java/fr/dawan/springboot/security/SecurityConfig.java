package fr.dawan.springboot.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import fr.dawan.springboot.services.implement.UserService;

@Configuration
@EnableWebSecurity
@SuppressWarnings("deprecation")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userDetailService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailService);
    }
    
    
    @Bean
   PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Bean
   AuthenticationProvider getProvider() {
        AppAuthProvider provider= new AppAuthProvider();
        provider.setUserDetailsService(userDetailService);
        return provider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
           http
           .csrf().disable()
           .authenticationProvider(getProvider())
           .authorizeRequests()
           .antMatchers(HttpMethod.POST).authenticated()
           .antMatchers(HttpMethod.PUT).authenticated()
           .antMatchers(HttpMethod.DELETE).authenticated()
           .anyRequest().permitAll()
           .and()
           .httpBasic();
    }

}
