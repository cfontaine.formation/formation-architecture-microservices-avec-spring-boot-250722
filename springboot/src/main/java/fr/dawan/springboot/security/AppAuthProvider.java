package fr.dawan.springboot.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import fr.dawan.springboot.services.implement.UserService;

public class AppAuthProvider extends DaoAuthenticationProvider {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private PasswordEncoder encoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken auth=(UsernamePasswordAuthenticationToken) authentication;
        String name=auth.getName();
        String password=auth.getCredentials().toString();
        System.out.println(name + " " + password);
        UserDetails user=userService.loadUserByUsername(name);
        if(password!=null && encoder.matches(password, user.getPassword())) {
            return new UsernamePasswordAuthenticationToken(user,null,null);
        }
        else {
            throw new BadCredentialsException("Mauvais mot de passe");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
    
    
    

}
