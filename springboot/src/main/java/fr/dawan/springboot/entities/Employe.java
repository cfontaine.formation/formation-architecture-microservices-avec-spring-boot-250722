package fr.dawan.springboot.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import fr.dawan.springboot.enums.Contrat;

@Entity
@Table(name = "employes")
//@TableGenerator(name="EmployeTableGen")
public class Employe extends AbstractEntity {

    private static final long serialVersionUID = 1L;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY) // ,generator = "EmployeTableGen"
//    private long id;
//    
    @Column(length = 60)
    private String prenom;

    @Column(length = 60, nullable = false)
    private String nom;

    @Column(nullable = false, name = "date_naissance")
    private LocalDate dateNaissance;

    @Enumerated(EnumType.STRING)
    @Column(length = 15, nullable = false)
    private Contrat contrat;

    @Lob
    private byte[] photo;

    @Embedded
    private Adresse adressePerso;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "rue", column = @Column(name = "rue_pro")),
            @AttributeOverride(name = "ville", column = @Column(name = "ville_pro")),
            @AttributeOverride(name = "codePostal", column = @Column(name = "code_postal_pro")), })
    private Adresse adressePro;

    @Transient
    private String nePasPerister;
    
//    @ElementCollection
//    @CollectionTable(name="employe_telephone", joinColumns= {@JoinColumn(name="phone_id")})
//    @MapKeyColumn(name="type_telephone")
//    @Column(name="numero_telephone")
//    private Map<String,String> telephones=new HashMap<>();
    @ElementCollection(targetClass = String.class)
    @CollectionTable(name="employe_telephone")
    private List<String> telephones=new ArrayList<>();
    
    public Employe() {

    }

    public Employe(String prenom, String nom, LocalDate dateNaissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
    }

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getNePasPerister() {
        return nePasPerister;
    }

    public void setNePasPerister(String nePasPerister) {
        this.nePasPerister = nePasPerister;
    }

    public Contrat getContrat() {
        return contrat;
    }

    public void setContrat(Contrat contrat) {
        this.contrat = contrat;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Adresse getAdressePerso() {
        return adressePerso;
    }

    public void setAdressePerso(Adresse adressePerso) {
        this.adressePerso = adressePerso;
    }

    public Adresse getAdressePro() {
        return adressePro;
    }

    public void setAdressePro(Adresse adressePro) {
        this.adressePro = adressePro;
    }


    public List<String> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<String> telephones) {
        this.telephones = telephones;
    }

    @Override
    public String toString() {
        return "Employe [prenom=" + prenom + ", nom=" + nom + ", dateNaissance=" + dateNaissance + ", contrat="
                + contrat + ", adressePerso=" + adressePerso + ", adressePro=" + adressePro + ", nePasPerister="
                + nePasPerister + ", " + super.toString() + "]";
    }

}
