package fr.dawan.springboot.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
public class SpectateurFilm {
    
    @EmbeddedId
    private SpectateurFilmPk id;
    
    @ManyToOne
    @MapsId("filmId")
    private Film film;
    
    @ManyToOne
    @MapsId("spectateurId")
    private Spectateur spectateur;
    
    private int note;

    public SpectateurFilm(Film film, Spectateur spectateur, int note) {
        this.film = film;
        this.spectateur = spectateur;
        this.note = note;
    }

    public SpectateurFilmPk getId() {
        return id;
    }

    public void setId(SpectateurFilmPk id) {
        this.id = id;
    }

    public Spectateur getSpectateur() {
        return spectateur;
    }

    public void setSpectateur(Spectateur spectateur) {
        this.spectateur = spectateur;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    
    
}
