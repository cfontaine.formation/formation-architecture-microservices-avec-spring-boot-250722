package fr.dawan.springboot.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
//@DiscriminatorValue("CE")
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;
    
    private double taux=1.5;

    public CompteEpargne() {
    }

    public CompteEpargne(double solde, String titulaire,double taux) {
        super(solde, titulaire);
        this.taux=taux;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    @Override
    public String toString() {
        return "CompteEpargne [taux=" + taux + ", toString()=" + super.toString() + "]";
    }

}
