package fr.dawan.springboot.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Adresse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private String rue;
    
    @Column(length=60,nullable=false)
    private String ville;
    
    @Column(length=8,nullable = false, name="code_postal")
    private String codePostal;

    public Adresse() {

    }

    public Adresse(String rue, String ville, String codePostal) {
        super();
        this.rue = rue;
        this.ville = ville;
        this.codePostal = codePostal;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    @Override
    public String toString() {
        return "Adresse [rue=" + rue + ", ville=" + ville + ", codePostal=" + codePostal + "]";
    }
}
