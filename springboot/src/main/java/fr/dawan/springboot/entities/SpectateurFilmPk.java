package fr.dawan.springboot.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class SpectateurFilmPk implements Serializable {

    private long filmId;
    
    private long spectateurId;

    public SpectateurFilmPk() {
    }

    public SpectateurFilmPk(long filmId, long spectateurId) {
        this.filmId = filmId;
        this.spectateurId = spectateurId;
    }

    public long getFilmId() {
        return filmId;
    }

    public void setFilmId(long filmId) {
        this.filmId = filmId;
    }

    public long getSpectateurId() {
        return spectateurId;
    }

    public void setSpectateurId(long spectateurId) {
        this.spectateurId = spectateurId;
    }
    
}
