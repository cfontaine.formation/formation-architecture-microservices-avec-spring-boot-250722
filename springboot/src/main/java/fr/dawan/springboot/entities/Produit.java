package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import fr.dawan.springboot.enums.Conditionnement;
@Entity
@Table(name="produits")
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(nullable = false)
    private String description;
    
    @Column(nullable = false)
    private double prix;
    
    @Column(nullable = false, name="date_conception")
    private LocalDate dateConception;
    
    @Enumerated(EnumType.STRING)
    private Conditionnement emballage;
    
    @Lob
    private byte[] image;
    
    @ManyToOne
    private Marque marque;
    
    @ManyToMany(mappedBy = "produits")
    private List<Fournisseur> fournisseurs=new ArrayList<>();

    public Produit() {
    }

    public Produit(String description, double prix, LocalDate dateConception, Conditionnement emballage) {
        this.description = description;
        this.prix = prix;
        this.dateConception = dateConception;
        this.emballage = emballage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public LocalDate getDateConception() {
        return dateConception;
    }

    public void setDateConception(LocalDate dateConception) {
        this.dateConception = dateConception;
    }

    public Conditionnement getEmballage() {
        return emballage;
    }

    public void setEmballage(Conditionnement emballage) {
        this.emballage = emballage;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public void setFournisseurs(List<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }

    @Override
    public String toString() {
        return "Produit [id=" + id + ", version=" + version + ", description=" + description + ", prix=" + prix
                + ", dateConception=" + dateConception + ", emballage=" + emballage + "]";
    }
    

}
