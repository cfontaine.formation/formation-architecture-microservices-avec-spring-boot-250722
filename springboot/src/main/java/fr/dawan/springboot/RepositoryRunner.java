package fr.dawan.springboot;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.entities.Produit;
import fr.dawan.springboot.enums.Conditionnement;
import fr.dawan.springboot.repositories.EmployeCustomRepository;
import fr.dawan.springboot.repositories.ProduitRepository;

//@Component
//@Order(value=1)
public class RepositoryRunner implements ApplicationRunner {

    @Autowired
    private ProduitRepository produitRepository;
    
    @Autowired 
    private EmployeCustomRepository empRepository;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Repository Runner");
        System.out.println("_________________________");
       List<Produit> lst= produitRepository.findAll();
       for(Produit p :lst) {
           System.out.println(p);
       }
       System.out.println("_________________________");
       System.out.println("nb produit= "+produitRepository.count());
       System.out.println("_________________________");
       lst=produitRepository.findByPrixLessThan(100.0);
       for(Produit p :lst) {
           System.out.println(p);
       }
       System.out.println("_________________________");
       lst=produitRepository.findByPrixBetween(20.0, 100.0);
       for(Produit p :lst) {
           System.out.println(p);
       }
       System.out.println("_________________________");
       lst=produitRepository.findByPrixGreaterThanAndEmballage(100.0, Conditionnement.CARTON);
       for(Produit p :lst) {
           System.out.println(p);
       }
       System.out.println("_________________________");
       lst=produitRepository.findByPrixLessThanJQPL(99.99);
       for(Produit p :lst) {
           System.out.println(p);
       }
       System.out.println("_________________________");
       lst=produitRepository.findByPrixLessThanSQL(99.99);
       for(Produit p :lst) {
           System.out.println(p);
       }
       System.out.println("_________________________");
       Page<Produit> pageProd=produitRepository.findByPrixLessThan(3000.0, PageRequest.of(0, 3)); //Pageable.unpaged()
       System.out.println(pageProd.getTotalElements() + " " + pageProd.getTotalPages() +"  "+pageProd.isFirst());
       lst=pageProd.getContent();
       for(Produit p :lst) {
           System.out.println(p);
       }
       System.out.println("_________________________");
       lst=produitRepository.findByPrixLessThan(200.0, Sort.by("description").and(Sort.by("prix").descending())); //Sort.unsorted()
       for(Produit p :lst) {
           System.out.println(p);
       }
       
       System.out.println("_________________________");
       pageProd=produitRepository.findByPrixLessThan(3000.0, PageRequest.of(1, 5,Sort.by("prix")));
       System.out.println(pageProd.getTotalElements() + " " + pageProd.getTotalPages() +"  "+pageProd.isFirst());
       lst=pageProd.getContent();
       for(Produit p :lst) {
           System.out.println(p);
       }
       System.out.println("_________________________");
       lst=produitRepository.findTop5ByOrderByPrix();
       for(Produit p :lst) {
           System.out.println(p);
       }
       
       System.out.println("_________________________");
       System.out.println(produitRepository.existsByPrixBetween(20.0, 50.0));
       System.out.println(produitRepository.existsByPrixBetween(2000.0, 5000.0));
       System.out.println(produitRepository.countByEmballage(Conditionnement.PLASTIQUE));
       System.out.println(produitRepository.countByEmballage(Conditionnement.SANS));
       System.out.println("_________________________");
       System.out.println(produitRepository.countByPrixSQL(30.0)); // SQL
       System.out.println(produitRepository.countByPrix(30.0));
       System.out.println(produitRepository.get_count_by_prix2(30.0));
       
       System.out.println("_________________________");
       empRepository.findBy("John", null).forEach(System.out::println); 

       System.out.println("_________________________");
       empRepository.findBy(null, "Dalton").forEach(System.out::println); 
 
       System.out.println("_________________________");
       empRepository.findBy("John", "Doe").forEach(System.out::println); 
    }

}
