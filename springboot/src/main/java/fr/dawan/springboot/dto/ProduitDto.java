package fr.dawan.springboot.dto;

import java.time.LocalDate;
import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.dawan.springboot.enums.Conditionnement;

@XmlRootElement(name="produit")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProduitDto {
    
    @XmlAttribute
    private long id;
    
    @XmlElement
    private double prix;
    
    @XmlElement
    private String description;
    
    @XmlElement
    private LocalDate dateConception;
    
    @XmlElement
    private Conditionnement emballage;
    @XmlElement
    private byte[] image;
    @XmlElement
    private MarqueDto marque;

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDateConception() {
        return dateConception;
    }

    public void setDateConception(LocalDate dateConception) {
        this.dateConception = dateConception;
    }

    public Conditionnement getEmballage() {
        return emballage;
    }

    public void setEmballage(Conditionnement emballage) {
        this.emballage = emballage;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
    public MarqueDto getMarque() {
        return marque;
    }

    public void setMarque(MarqueDto marque) {
        this.marque = marque;
    }

    @Override
    public String toString() {
        return "ProduitDto [id=" + id + ", prix=" + prix + ", description=" + description + ", dateConception="
                + dateConception + ", emballage=" + emballage + ", image=" + Arrays.toString(image) + ", marque="
                + marque + "]";
    }


}
