package fr.dawan.springboot.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.dto.ProduitDto;
import fr.dawan.springboot.entities.Produit;

@Component
public class ProduitMapper {
    
    private ModelMapper modelMapper=new ModelMapper();

    public ProduitDto toDto(Produit produit) {
        modelMapper.typeMap(Produit.class,ProduitDto.class).addMappings(
                mapper ->{
                    mapper.map(src -> src.getId(), ProduitDto::setId);
                    mapper.map(src -> src.getDescription(), ProduitDto::setDescription);
                    mapper.map(src -> src.getDateConception(), ProduitDto::setDateConception);
                    mapper.map(src -> src.getPrix(), ProduitDto::setPrix);
                    mapper.map(src -> src.getEmballage(), ProduitDto::setEmballage);
                    mapper.map(src -> src.getMarque(), ProduitDto::setMarque);
                    mapper.map(src -> src.getImage(), ProduitDto::setImage);
                });
         return modelMapper.map(produit, ProduitDto.class);
    }
    
    
    public Produit fromDto(ProduitDto produitDto) {
        modelMapper.typeMap(ProduitDto.class,Produit.class).addMappings(
                mapper ->{
                    mapper.map(src -> src.getId(), Produit::setId);
                    mapper.map(src -> src.getDescription(), Produit::setDescription);
                    mapper.map(src -> src.getDateConception(), Produit::setDateConception);
                    mapper.map(src -> src.getPrix(), Produit::setPrix);
                    mapper.map(src -> src.getEmballage(), Produit::setEmballage);
                    mapper.map(src -> src.getMarque(), Produit::setMarque);
                    mapper.map(src -> src.getImage(), Produit::setImage);
                });
         return modelMapper.map(produitDto, Produit.class);
    }

}
