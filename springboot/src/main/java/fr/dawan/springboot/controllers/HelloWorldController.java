package fr.dawan.springboot.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
public class HelloWorldController {
    
    @Value("${msg:defaultmessage}")
    private String message;
    
    @Operation(hidden = true)
    @GetMapping("/hello")
    public String helloWorld() {
        return message + " ----";
    }

}
