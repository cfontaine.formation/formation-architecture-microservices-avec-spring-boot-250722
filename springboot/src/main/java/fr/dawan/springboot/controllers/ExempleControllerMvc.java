package fr.dawan.springboot.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/mvc")
public class ExempleControllerMvc {
    
    @GetMapping("/freemarker/{message}")
    public String exempleVueFtlh(@PathVariable String message,Model model) {
        model.addAttribute("msg",message);
        return "exemplevue";
    }
    
    @GetMapping("/jsp/{message}")
    public String exempleVueJsp(@PathVariable String message,Model model) {
        model.addAttribute("msg",message);
        return "exemplevuejsp";
    }
}
