package fr.dawan.springboot.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.dawan.springboot.dto.ProduitDto;
import fr.dawan.springboot.services.ProduitService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/produits")
@Tag(name="Produit", description="l'api des __produits__")
public class ProduitController {
    
    @Autowired
    ProduitService service;
    
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE) //value="",
    public  List<ProduitDto> getAllProduit(){
        return service.getAllProduit(Pageable.unpaged());
    }

    @GetMapping(params= {"page","size"},produces=MediaType.APPLICATION_JSON_VALUE)
    public  List<ProduitDto> getAllProduit(Pageable page){
        return service.getAllProduit(page);
    }
    
    @Operation(summary = "Trouver les produits avec leur id",description = "retourne un produit", tags= {"Produits"})
    @ApiResponses(value= {
            @ApiResponse(responseCode = "200" , description="opération reussit",
               content = @Content(schema = @Schema(implementation=ProduitDto.class)))
    })
    @GetMapping(value="/{id}",produces=MediaType.APPLICATION_XML_VALUE)
    public ProduitDto getProduitById(
            @Parameter(description = "id du produit que l'on veut obtenir",required = true,allowEmptyValue = false)
            @PathVariable long id) {
        return service.getProduitById(id);
    }
    
    @DeleteMapping(value= "/{id}",produces=MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> deleteProduit(@PathVariable long id) {
        try {
            service.deleteProduit(id);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        // return "Le produit id=" + id + " est supprimé";
    }
    
    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    public ProduitDto ajoutProduit(@RequestBody ProduitDto produitDto) {
        return service.saveOrUpdate(produitDto);
    }
    
    @PostMapping(value="/image/{id}", consumes=MediaType.MULTIPART_FORM_DATA_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public ProduitDto ajoutImageProduit(@RequestParam("image") MultipartFile file,@PathVariable long id) throws IOException {
        ProduitDto pr=service.getProduitById(id);
        System.out.println(file.getSize() + " " + file.getOriginalFilename() + " " +file.getName());
        pr.setImage(file.getBytes());
        return service.saveOrUpdate(pr);
    }
    
    @PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    public ProduitDto modifierProduit(@RequestBody ProduitDto produitDto) {
        return service.saveOrUpdate(produitDto);
    }
    
    @ResponseStatus(code=HttpStatus.NOT_FOUND)
    @ExceptionHandler({IOException.class})
    public String gestionIoException(IOException e) {
        return e.getMessage();
    }
    
    @GetMapping("/exception/io")
    public void genIOException() throws IOException {
        throw new IOException("Traitement produit echoué");
    }
    
    @GetMapping("/exception")
    public void genException() throws Exception {
        throw new Exception("Traitement produit echoué");
    }
    
//  @PutMapping(value="/{id}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
//  public ProduitDto modifierProduit2(@PathVariable long id,@RequestBody ProduitDto produitDto) {
//     ProduitDto p=service.getProduitById(id);
//     p.setDescription(produitDto.getDescription());
//     p.setPrix(produitDto.getPrix());
//     p.setDateConception(produitDto.getDateConception());
//     p.setEmballage(produitDto.getEmballage());
//     p.setImage(produitDto.getImage());
//     p.setMarque(produitDto.getMarque());
//     return service.saveOrUpdate(p);
//  }
}
