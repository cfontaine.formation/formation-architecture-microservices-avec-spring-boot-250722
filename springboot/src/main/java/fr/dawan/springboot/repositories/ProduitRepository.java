package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.springboot.entities.Produit;
import fr.dawan.springboot.enums.Conditionnement;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {

    // sujet find
    List<Produit> findByPrixLessThan(double prix);

    List<Produit> findByPrixBetween(double prixMin, double prixMax);

    List<Produit> findByPrixGreaterThanAndEmballage(double prix, Conditionnement emballage);

    List<Produit> findByPrixLessThanOrderByPrixDesc(double prix);

    List<Produit> findByDescriptionLike(String model);
    
    List<Produit> findByEmballage(Conditionnement emballage);

    // sujet Exist
    boolean existsByPrixBetween(double min, double max);

    // sujet Count
    int countByEmballage(Conditionnement emballage);
    
    // JPQL
    // @Query(value="SELECT p FROM Produit p WHERE p.prix<:montant")
    @Query("FROM Produit p WHERE p.prix<:montant") // ?1
    List<Produit> findByPrixLessThanJQPL(@Param("montant") double prixMax);
    
    // SQL
    @Query(nativeQuery = true, value="SELECT * FROM produits WHERE prix<:montant")
    List<Produit> findByPrixLessThanSQL(@Param("montant") double prixMax);
    
    // Pagination
    Page<Produit> findByPrixLessThan(double prix,Pageable pageable);
    
    List<Produit> findByPrixLessThan(double prix,Sort sort);
    
    // LIMIT ou TOP
    List<Produit>findTop5ByOrderByPrix();
    
    // Procédure Stockée
    // SQL
    @Query(nativeQuery = true,value="CALL GET_COUNT_BY_PRIX(:prixMax)")
    int countByPrixSQL(@Param("prixMax")double prixMax);
    
    // Appel Explicite
    @Procedure("GET_COUNT_BY_PRIX2")
    int countByPrix(double montant);
    
    // Appel Implicite
    @Procedure
    int get_count_by_prix2(@Param("montant") double montant);
}
