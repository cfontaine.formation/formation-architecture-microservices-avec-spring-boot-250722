package fr.dawan.springboot.services.implement;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.dawan.springboot.dto.ProduitDto;
import fr.dawan.springboot.entities.Produit;
import fr.dawan.springboot.enums.Conditionnement;
import fr.dawan.springboot.mapper.ProduitMapper;
import fr.dawan.springboot.repositories.ProduitRepository;
import fr.dawan.springboot.services.ProduitService;

@Service
@Transactional
public class ProduitServiceImpl implements ProduitService {

    @Autowired
    private ProduitRepository produitRepository;

//    @Autowired
//    private ModelMapper mapper;
    @Autowired
    private ProduitMapper prodMapper;

    @Override
    public List<ProduitDto> getAllProduit(Pageable page) {
//        List<Produit> lst=produitRepository.findAll(page).getContent();
//        List<ProduitDto> lstDto=new ArrayList<>();
//        for(Produit p :lst) {
//            lstDto.add(mapper.map(p, ProduitDto.class));
//        }
//        return lstDto;
 //       return produitRepository.findAll(page).get().map(m -> mapper.map(m,ProduitDto.class)).collect(Collectors.toList());
        
        return produitRepository.findAll(page).get().map(prodMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public ProduitDto getProduitById(long id) {
        //Produit pr = produitRepository.findById(id).get();
       // return mapper.map(pr, ProduitDto.class);
        return prodMapper.toDto(produitRepository.findById(id).get());
    }

    @Override
    public List<ProduitDto> getProduitByEmballage(Conditionnement emballage) {
     //   return produitRepository.findByEmballage(emballage).stream().map(m -> mapper.map(m, ProduitDto.class)).collect(Collectors.toList());
        return produitRepository.findByEmballage(emballage).stream().map(prodMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public ProduitDto saveOrUpdate(ProduitDto produitDto) {
//        Produit tmp=produitRepository.saveAndFlush(mapper.map(produitDto, Produit.class));
//        return mapper.map(tmp,ProduitDto.class);
        Produit tmp=produitRepository.saveAndFlush(prodMapper.fromDto(produitDto));
        return prodMapper.toDto(tmp);
    }

    @Override
    public void deleteProduit(long id) {
        produitRepository.deleteById(id);

    }

}
