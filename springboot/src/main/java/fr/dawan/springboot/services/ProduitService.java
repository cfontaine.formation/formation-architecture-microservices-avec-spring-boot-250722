package fr.dawan.springboot.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dto.ProduitDto;
import fr.dawan.springboot.enums.Conditionnement;

public interface ProduitService {

    List<ProduitDto> getAllProduit(Pageable page);

    ProduitDto getProduitById(long id);

    List<ProduitDto> getProduitByEmballage(Conditionnement emballage);

    ProduitDto saveOrUpdate(ProduitDto produit);

    void deleteProduit(long id);

}
