package fr.dawan.springboot.services.implement;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.dawan.springboot.repositories.UserRepository;

@Service
public class UserService implements UserDetailsService {
    
    private UserRepository useRepository;

    public UserService(UserRepository useRepository) {
        this.useRepository = useRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
      return useRepository.findByUsername(username).orElseThrow(() ->new UsernameNotFoundException("Utilisateur non trouvé"));
    }

}
