package fr.dawan.springboot;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.dto.ProduitDto;
import fr.dawan.springboot.enums.Conditionnement;
import fr.dawan.springboot.services.ProduitService;

//@Component
//@Order(value=2)
public class ServiceRunner implements ApplicationRunner {

    
    @Autowired
    private ProduitService prodService;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
      System.out.println("ServiceRunner");
       
      ProduitDto p1=prodService.getProduitById(2);
      System.out.println(p1);
      
      List<ProduitDto> lst=prodService.getAllProduit(Pageable.unpaged());
      lst.forEach(System.out::println);
      
      
      prodService.getProduitByEmballage(Conditionnement.SANS).forEach(System.out::println);

      
      ProduitDto p2=new ProduitDto();
      p2.setDescription("Alimentation portable 130W");
      p2.setEmballage(Conditionnement.SANS);
      p2.setDateConception(LocalDate.of(2018,3 ,10));
      p2.setPrix(80.0);
      
      p2=prodService.saveOrUpdate(p2);
      System.out.println(p2);
      
      prodService.deleteProduit(p2.getId());
      
      lst=prodService.getAllProduit(Pageable.unpaged());
      lst.forEach(System.out::println);
      
    }

}
