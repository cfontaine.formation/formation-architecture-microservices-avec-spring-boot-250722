package fr.dawan.bibliotheque;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import fr.dawan.bibliotheque.entities.Auteur;
import fr.dawan.bibliotheque.entities.Livre;
import fr.dawan.bibliotheque.repositories.AuteurRepository;
import fr.dawan.bibliotheque.repositories.LivreRepository;

//@Component
public class RunnerRepository implements ApplicationRunner {
    
    @Autowired
    private LivreRepository livreRepository;
    
    @Autowired
    private AuteurRepository auteurRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
       
        List<Livre> lstLivre=livreRepository.findByTitreLike("D___%");
        lstLivre.forEach(System.out::println);
        
        lstLivre=livreRepository.findByAnneeSortie(1992);
        for(Livre l : lstLivre ) {
            System.out.println(l);
        }
        
        lstLivre=livreRepository.findByAnneeSortieBetweenOrderByAnneeSortie(1970, 1980);
        for(Livre l : lstLivre ) {
            System.out.println(l);
        }
        

        livreRepository.findByCategorieNom("Drame").forEach(System.out::println);
        
        
        
        List<Auteur> lstAuteur=auteurRepository.findByDecesIsNull();
        for(Auteur a : lstAuteur) {
            System.out.println(a);
        }
        
        Page<Auteur>pageAuteur=auteurRepository.findByDecesIsNull(PageRequest.of(1, 2));
        pageAuteur.forEach(System.out::println);
        
        lstAuteur=auteurRepository.findByLivreId(1);
        lstAuteur.forEach(System.out::println);
        
        lstAuteur=auteurRepository.findByLivreId(132);
        lstAuteur.forEach(System.out::println);
        
        auteurRepository.findByTopAuteur().forEach(System.out::println);
    }

}
