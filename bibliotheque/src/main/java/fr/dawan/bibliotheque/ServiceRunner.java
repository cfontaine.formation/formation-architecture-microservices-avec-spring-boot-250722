package fr.dawan.bibliotheque;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import fr.dawan.bibliotheque.dto.LivreDto;
import fr.dawan.bibliotheque.services.LivreService;

@Component
public class ServiceRunner implements ApplicationRunner {
    
    @Autowired
    LivreService service;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<LivreDto> lst=service.getAllLivre(PageRequest.of(0,10));
        lst.forEach(System.out::println);
        
        System.out.println(service.getById(142));
        
        lst=service.searchByTitre("Dune");
        lst.forEach(System.out::println);
        
        
    }

}
