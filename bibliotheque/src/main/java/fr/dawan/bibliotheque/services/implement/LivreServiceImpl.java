package fr.dawan.bibliotheque.services.implement;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.dawan.bibliotheque.dto.LivreDto;
import fr.dawan.bibliotheque.entities.Livre;
import fr.dawan.bibliotheque.repositories.LivreRepository;
import fr.dawan.bibliotheque.services.LivreService;

@Service
public class LivreServiceImpl implements LivreService {

    @Autowired
    private LivreRepository livreRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<LivreDto> getAllLivre(Pageable page) {
        return livreRepository.findAll(page).get().map(l -> modelMapper.map(l, LivreDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public LivreDto getById(long id) {
        return modelMapper.map(livreRepository.findById(id).get(), LivreDto.class);
    }

    @Override
    public List<LivreDto> searchByTitre(String titre) {
        return livreRepository.findByTitreLike("%" + titre + "%").stream().map(l -> modelMapper.map(l, LivreDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(long id) {
        livreRepository.deleteById(id);
    }

    @Override
    public LivreDto saveOrUpdate(LivreDto livreDto) {
        Livre tmp = livreRepository.saveAndFlush(modelMapper.map(livreDto, Livre.class));
        return modelMapper.map(tmp, LivreDto.class);
    }

}
