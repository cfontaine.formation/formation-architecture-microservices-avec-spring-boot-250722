package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.bibliotheque.entities.Livre;

public interface LivreRepository extends JpaRepository<Livre, Long> {

    List<Livre> findByTitreLike(String modele);
    
    List<Livre> findByAnneeSortie(int annee);
    
    List<Livre> findByAnneeSortieBetweenOrderByAnneeSortie(int anneeMin,int anneMax);

//    @Query("FROM Livre l JOIN l.categorie c WHERE c.nom=:nomCategorie")
//    List<Livre> findByNomCategorie(@Param("nomCategorie")String nom);
    List<Livre> findByCategorieNom(String nom);
    
}
